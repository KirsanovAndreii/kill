package com.example.ankir.self_teacherofgeography;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityCheckIn extends AppCompatActivity {
    Md5 md5 = new Md5();
    User userCurrent;
    DataBaseMaster dbMaster;
    @BindView(R.id.a_checkin_login)
    EditText checkInLogin;
    @BindView(R.id.a_checkin_pass)
    EditText checkInPass;
    @BindView(R.id.a_checkin_country)
    EditText checkInCountry;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.a_checkin_click_save)
    void onClickSave() {
        dbMaster = DataBaseMaster.getInstance(this);
        int currentIdSR;

        if (checkInLogin.getText().length() == 0) {
            Toast.makeText(ActivityCheckIn.this, "пустой login", Toast.LENGTH_SHORT).show();
        } else {
            userCurrent = dbMaster.getUser(checkInLogin.getText().toString());
            if (userCurrent != null) {
                Toast.makeText(ActivityCheckIn.this, "имя уже занято", Toast.LENGTH_SHORT).show();
            } else {
                if (checkInPass.getText().length() < 5) {
                    Toast.makeText(ActivityCheckIn.this, "password короче 5 символов", Toast.LENGTH_SHORT).show();
                } else {
                    if ((dbMaster.
                            getIdSubRregionByCountry(checkInCountry.getText().toString())) == -1) {
                        Toast.makeText(ActivityCheckIn.this, "нет такой страны", Toast.LENGTH_SHORT).show();
                    } else {

                        dbMaster.insertUser(new User(
                                checkInLogin.getText().toString(),
                                md5.md5(checkInPass.getText().toString()),
                                checkInCountry.getText().toString()));

                        userCurrent = dbMaster.getUser(checkInLogin.getText().toString());
                        Log.d("checkin_login", userCurrent.toString());

                        ActivityQuestionnaire.currentLogin = userCurrent.id; //id login для вопросов
                        createQuestionForUser(dbMaster.
                                getIdSubRregionByCountry(checkInCountry.getText().toString()));
                        this.onBackPressed();
                    }
                }
            }
        }


    } //onClickSave()

    public void createQuestionForUser(int idSubregion) {
        List<ResponseCountries> listCountriesSRegion = dbMaster.getCountries(idSubregion);

        //проверка в логе
        for (ResponseCountries countries : listCountriesSRegion) {
            Log.d("Country for User ", countries.toString());
        }
// в таблицу вопросов добавляем по каждой стране 4 вопроса с 0-м результатом
        for (ResponseCountries countries : listCountriesSRegion) {
            dbMaster.insertQuestion(userCurrent.id, countries.idBySQL,
                    ActivityQuestionnaire.questionID_1);
            dbMaster.insertQuestion(userCurrent.id, countries.idBySQL,
                    ActivityQuestionnaire.questionID_2);
            dbMaster.insertQuestion(userCurrent.id, countries.idBySQL,
                    ActivityQuestionnaire.questionID_3);
            dbMaster.insertQuestion(userCurrent.id, countries.idBySQL,
                    ActivityQuestionnaire.questionID_4);
        }
        Log.d("Question created ", "Ok");

    }

}
