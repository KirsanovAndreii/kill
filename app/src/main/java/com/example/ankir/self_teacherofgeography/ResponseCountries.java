package com.example.ankir.self_teacherofgeography;

/**
 * Created by ankir on 13.06.2017.
 */

public class ResponseCountries {
    int idBySQL; // используется в sql запросах
    String name;
    String capital;
    String region;
    String subregion;
    int population;

    public ResponseCountries() {

    }

    public ResponseCountries(String name, String capital, String region, String subregion, int population) {
        this.idBySQL = 0;
        this.name = name;
        this.capital = capital;
        this.region = region;
        this.subregion = subregion;
        this.population = population;
    }

    @Override
    public String toString() {
        return "ResponseCountries{" +
                "name='" + name + '\'' +
                ", capital='" + capital + '\'' +
                ", region='" + region + '\'' +
                ", subregion='" + subregion + '\'' +
                ", population=" + population +
                '}';
    }
}
