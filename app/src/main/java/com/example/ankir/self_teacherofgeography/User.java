package com.example.ankir.self_teacherofgeography;

/**
 * Created by ankir on 16.06.2017.
 */

public class User {
    int id;
    String login;
    String pass;
    String country;

    public User(String login, String pass, String country) {
        id = 0;
        this.login = login;
        this.pass = pass;
        this.country = country;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", pass='" + pass + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
